package exo13;

import java.util.HashMap;
import java.util.Map;

public class Exo13 {

	public static void main(String[] args) {
		Map<Integer, String> fixedMap = Map.ofEntries(
				Map.entry(3, "one"),
				Map.entry(4, "four"),
				Map.entry(5, "three"),
				Map.entry(6, "eleven")
				);

		Map<Integer, String> map = new HashMap<>(fixedMap);
		System.out.println("The fixedMap is :");
		map.forEach((key,value) -> System.out.println(key + " = " + value));
		map.replaceAll((key,value) -> value.toUpperCase());
		System.out.println("\nmap en majuscule ");
		map.forEach((key,value) -> System.out.println(key + " = " + value));
     }
}