package exo12;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;



public class Exo12 {

public static void main(String[] args)throws CloneNotSupportedException  {
		
		List<String> Words = 
				Arrays.asList(
						"tnne", "two", "three", "four", "five", 
						"six", "seven", "eight", "nine", "ten",
						"eleven", "twelve");
		System.out.println("\nWords:");
		Words.forEach(System.out::println);
		
		
		Predicate<String> paire =  s ->  s.length() % 2 == 0;
		System.out.println("\nChaines de longueurs paires");
		for (String s : Words) {
			System.out.println(s + " de longueur paire ? " + paire.test(s));
		}
		
		
		
		List<String> NewWords = new ArrayList<>(Words);
	     NewWords.removeIf(paire );
	     
		System.out.println("\nafter removeIf paire");
		NewWords.forEach(s -> System.out.println(s));
		
		
		System.out.println("\n Words en majuscule ");
		
		Function<String, String> majscule =  s -> s.toUpperCase();
		for (String s2 : Words) {
			System.out.println(majscule.apply(s2));
		}
		System.out.println("\nWords begin with voyelle ?? ");
		Predicate<String> Voyelle = s -> s.startsWith("a")||s.startsWith("e")||s.startsWith("i")||s.startsWith("o")||s.startsWith("u")||s.startsWith("y");
		for (String s : Words) {
			System.out.println(Voyelle.test(s));
		}
		System.out.println("\nWords begin with voyelle en majuscule ");
		for (String s : Words) {
			if (Voyelle.test(s))
			System.out.println(majscule.apply(s));
			else 
				System.out.println(s);
		}
		
		Comparator<String> cmpByLength =Comparator.comparing(String::length);
		Words.sort(cmpByLength);
		System.out.println("\ncompare length:");
		Words.forEach(s -> System.out.println(s));
		
		Comparator<String> cmpBylengthandalphabet=cmpByLength.thenComparing(String::toString);
		Words.sort(cmpBylengthandalphabet);
		System.out.println("\ncompare length and then in  alphabetique order :");
		Words.forEach(s -> System.out.println(s));	
		
}
}

